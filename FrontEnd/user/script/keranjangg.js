document.addEventListener('DOMContentLoaded', function(){
    // Hapus Pesanan Yang Tidak Jadi
    const hapusPesan = document.getElementsByClassName('btn-danger');
    for (let i = 0; i < hapusPesan.length; i++) {
        const tombol = hapusPesan[i];
        tombol.addEventListener('click', hapusItem)
    }

    // Ambil Nilai Jumlah Menu Yang Di Beli
    const jumlahInput = document.getElementsByClassName('cart-quantity-input');
    for (let i = 0; i < jumlahInput.length; i++) {
        const input = jumlahInput[i];
        input.addEventListener('change', ubahJumlah)
    }

    // // Tekan Pilih dan Masuk ke Keranjang
    // const masukKeranjang = document.getElementsByClassName('shop-item-button');
    // console.log(masukKeranjang);
    // for (let i = 0; i < masukKeranjang.length; i++) {
    //     const tombol = masukKeranjang[i];
    //     tombol.addEventListener('click', tambahKeKeranjang)
    // }

    // // function dari addEventListener
    // function tambahKeKeranjang(event) {
    //     const tombol = event.target;
    //     const menuItem = tombol.parentElement.parentElement;
    //     const namaMenu = menuItem.getElementsByClassName('shop-item-title')[0].innerText;
    //     console.log(namaMenu);
    // }

    function hapusItem(event) {
     const tombolKlik = event.target;
     tombolKlik.parentElement.parentElement.remove();
     updateTotalHarga();
    }

    function ubahJumlah(event) {
        const input = event.target;
        if (isNaN(input.value) || input.value <= 0) {
            input.value = 1;
        }
        updateTotalHarga();
    }

    function updateTotalHarga() {
        const cartItemContainer = document.getElementsByClassName('cart-items')[0];
        const cartRows = cartItemContainer.getElementsByClassName('cart-row');
        let total = 0;
        for (let i = 0; i < cartRows.length; i++) {
            const cartRow = cartRows[i];
            const hargaMenu = cartRow.getElementsByClassName('cart-price')[0];
            const jumlahBeli = cartRow.getElementsByClassName('cart-quantity-input')[0]
            const harga = parseInt(hargaMenu.innerText.replace('Rp', ''));
            const jumlah = jumlahBeli.value;
            total = total + (harga * jumlah);
            // console.log(total);
        }
        document.getElementsByClassName('cart-total-price')[0].innerText = 'Rp. ' + total;
    }

})