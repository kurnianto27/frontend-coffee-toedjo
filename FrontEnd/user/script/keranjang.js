document.addEventListener('DOMContentLoaded', function(){
    ///// AWAL FETCH /////
    fetch('http://127.0.0.1:5010/menu-stock')
    .then(response => response.json())
    .then(menus => {
        menus.forEach(menu => {
            const divItem = document.createElement("div");
            divItem.setAttribute("class", "shop-item");
            const spanJudul = document.createElement("span");
            spanJudul.setAttribute("class", "shop-item-title");
            spanJudul.innerText = `${menu.menu}`;
            const img = document.createElement("img");
            img.setAttribute("class", "shop-item-image");
            img.setAttribute("src", `../img/${menu.menu_id}.jpg`);
            const divItemDetails = document.createElement("div");
            divItemDetails.setAttribute("class", "shop-item-details");
            divItem.appendChild(spanJudul);
            divItem.appendChild(img);
            divItem.appendChild(divItemDetails);
    
            const spanHarga = document.createElement("span");
            spanHarga.setAttribute("class", "shop-item-price");
            spanHarga.innerText = `Rp. ${menu.price}`;
            const tombol = document.createElement("button");
            tombol.setAttribute("class", "btn");
            tombol.classList.add("btn-primary");
            tombol.classList.add("shop-item-button");
            tombol.setAttribute("type", "button");
            tombol.innerText = "Pilih";
            divItemDetails.appendChild(spanHarga);
            divItemDetails.appendChild(tombol);
    
            // ambil parent
            const divParentUtama = document.getElementsByClassName("shop-items")[0];
            divParentUtama.appendChild(divItem);
        });

        // Dalam Fecth Aksi Jika Tekan Pilih //
        const masukKeranjang = document.getElementsByClassName('shop-item-button');
        for (let i = 0; i < masukKeranjang.length; i++) {
            const tombol = masukKeranjang[i];
            tombol.addEventListener('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                const menuItem = tombol.parentElement.parentElement
                const menu = menuItem.getElementsByClassName('shop-item-title')[0].innerText;
                const harga = menuItem.getElementsByClassName('shop-item-price')[0].innerText;
                const gambar = menuItem.getElementsByClassName('shop-item-image')[0].src;
                
                const cartItemNames = document.getElementsByClassName('cart-item-title');
                for (let i = 0; i < cartItemNames.length; i++) {
                    if (cartItemNames[i].innerHTML == menu) {
                        alert('whoops, menu sudah ada di keranjang kaka')
                        return
                    }}
                // bikin tempat cartnya
                const cartRow = document.createElement('div');
                cartRow.setAttribute("class", "cart-row");
                const cartRowContent = `
                                        <div class="cart-item cart-column">
                                            <img class="cart-item-image" src="${gambar}" width="100" height="100">
                                            <span class="cart-item-title">${menu}</span>
                                        </div>
                                        <span class="cart-price cart-column">${harga}</span>
                                        <div class="cart-quantity cart-column">
                                            <input class="cart-quantity-input" type="number" value="1">
                                            <button class="btn btn-danger" type="button">Hapus</button>
                                        </div>`
                cartRow.innerHTML = cartRowContent;
                // ambil parent
                const cartItems = document.getElementsByClassName('cart-items')[0];
                cartItems.appendChild(cartRow);
                alert('lihat menu yang dipesan kakak di bagian bawah ya');
                updateTotalHarga();
                
                // Tekan Pilih dan Masuk Keranjang //
                // Hapus Pesan Di Keranjang
                const hapusPesan = document.getElementsByClassName('btn-danger'); 
                for (let i = 0; i < hapusPesan.length; i++) {
                    const tombol = hapusPesan[i];
                    tombol.addEventListener('click', hapusItem)
                }

                function hapusItem(event) {
                    const tombolKlik = event.target;
                    tombolKlik.parentElement.parentElement.remove();
                    updateTotalHarga();
                }
                
                // Ambil Jumlah Pemesanaan Menu
                const jumlahInput = document.getElementsByClassName('cart-quantity-input');
                for (let i = 0; i < jumlahInput.length; i++) {
                    const input = jumlahInput[i];
                    input.addEventListener('change', function(e){
                        const input = e.target;
                        // console.log(input);
                        if (isNaN(input.value) || input.value <= 0) {
                            input.value = 1;
                        }
                        updateTotalHarga();
                    })
                }

                function updateTotalHarga() {
                    const cartItemContainer = document.getElementsByClassName('cart-items')[0];
                    const cartRows = cartItemContainer.getElementsByClassName('cart-row');
                    let total = 0;
                    for (let i = 0; i < cartRows.length; i++) {
                        const cartRow = cartRows[i];
                        const hargaMenu = cartRow.getElementsByClassName('cart-price')[0];
                        const jumlahBeli = cartRow.getElementsByClassName('cart-quantity-input')[0]
                        const harga = parseFloat(hargaMenu.innerText.replace('Rp.', ''));
                        // console.log(harga);
                        const jumlah = jumlahBeli.value;
                        total = total + (harga * jumlah);
                        // console.log(total);
                    }
                    document.getElementsByClassName('cart-total-price')[0].innerText = 'Rp. ' + total;
                }
            })
        }
    })
    ///// AKHIR FETCH /////

    // Kirim Pesanan
    const beliSekarang = document.getElementsByClassName('btn-purchase')[0];
    beliSekarang.addEventListener('click', function(e){
        e.preventDefault();
        e.stopPropagation();

        const jumlahBeli = document.getElementsByClassName('cart-quantity-input');
        let dataJumlahBeli = [];
        for (let i = 0; i < jumlahBeli.length; i++) {
            const data = jumlahBeli[i].value;
            dataJumlahBeli.push(parseInt(data));
        }   
        console.log(dataJumlahBeli);

        const menu = document.getElementsByClassName('cart-item-image');
        let dataMenuId = [];
        for (let i = 0; i < menu.length; i++) {
            const data = menu[i].src;
            const dataSend = data.slice(26, 28);
            dataMenuId.push(dataSend);
        }   // console.log(dataMenuId);

        const session = sessionStorage.getItem('sessionUser');
        let data = JSON.parse(session);
        const userIdSession = data[0].user_id;

        const pesananUser = {
            user_id : userIdSession,
            menu_id : dataMenuId,
            quantity : dataJumlahBeli
        }
        
        fetch('http://127.0.0.1:5010/user-order', {
            method : "POST",
            headers : {
                'Content-Type': 'application/json'
            },
            body : JSON.stringify(pesananUser)
        }).then(response => {
            response.json();
            const resp = response.status
            if(resp == 200){
                alert('Terimakasih telah pesan menu kami');
                window.location ='http://127.0.0.1:8000/user/dashboard.html';
            }
            if(resp == 401){
                alert(`
                Whoops maksimum pemesanan telah terlampaui...
                Tunggu pesanan selesai ya kak :)`);
                window.location ='http://127.0.0.1:8000/user';
            }
        })
    })
})